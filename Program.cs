﻿using System.IO;
namespace Sand
{
    class Program
    {
        public static double[,] MI = new double[11, 11];
        static void Main(string[] args)
        {
            string path = "../../res.txt";

            Class1.ReadData();
            /*
            using (StreamWriter sr = new StreamWriter("../../res_Mix.txt"))
            {
                for (int i = 0; i < 11; i++)
                    for (int j = i; j < 11; j++)
                    {
                        //sr.WriteLine(class1.MI(class1.rows, i, j).ToString() + "\t\t" + class1.Mix(i, j).ToString());
                        MI[i, j] = class1.Mix(i, j);
                        //MI[i, j] = class1.MI(class1.rows, i, j);
                        MI[j, i] = MI[i, j];
                    }

                for (int i = 0; i < 11; i++)
                {
                    for (int j = 0; j < 11; j++)
                        sr.Write(MI[i, j].ToString() + "\t");
                    sr.WriteLine(" ");
                }

            }*/
            using (StreamWriter sr = new StreamWriter(path))
            {
                for (int i = 0; i < 11; i++)
                    for (int j = i; j < 11; j++)
                    {
                        //sr.WriteLine(class1.MI(class1.rows, i, j).ToString() + "\t\t" + class1.Mix(i, j).ToString());
                        //MI[i, j] = class1.Mix(i, j);
                        MI[i, j] = Class1.MI(Class1.rows, i, j);
                        MI[j, i] = MI[i, j];
                    }
                for (int i = 0; i < 11; i++)
                {
                    for (int j = 0; j < 11; j++)
                        sr.Write(MI[i, j].ToString() + "\t");
                    sr.WriteLine(" ");
                }
            }
            Class2.Beis();
            using (StreamWriter sr = new StreamWriter("../../Simmetr_coef.txt"))
            {
                Class2.Build_coef();

                for (int i = 0; i < 11; i++)
                {
                    for (int j = 0; j < 11; j++)
                        sr.Write(Class2.Simmetric_coef[i, j].ToString() + "\t");
                    sr.WriteLine(" ");
                }
            }


        }
    }
}
