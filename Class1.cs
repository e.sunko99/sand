﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sand
{
   
    class Row
    {
        public double[] data = new double[11];
        public double BounceRates=>data[0];
        public double Weekend => data[1];
        public double Month => data[2];
        public double Administrat => data[3];
        public double ProductRela => data[4];
        public double PageValues=>data[5];
        public double Region    =>data[6];
        public double ExitRates => data[7];
        public double Browser => data[8];
        public double VisitorType => data[9];
        public double revenue => data[10];
        public Row(string row)
        {
            data[0] = Convert.ToDouble(row.Split('\t')[0]);
            data[1] = Convert.ToDouble(row.Split('\t')[1]);
            data[2] = Convert.ToDouble(row.Split('\t')[2]);
            data[3] = Convert.ToDouble(row.Split('\t')[3]);
            data[4] = Convert.ToDouble(row.Split('\t')[4]);
            data[5] = Convert.ToDouble(row.Split('\t')[5]);
            data[6] = Convert.ToDouble(row.Split('\t')[6]);
            data[7] = Convert.ToDouble(row.Split('\t')[7]);
            data[8] = Convert.ToDouble(row.Split('\t')[8]);
            data[9] = Convert.ToDouble(row.Split('\t')[9]);
            data[10] = Convert.ToDouble(row.Split('\t')[10]);
        }
        public Row(Row row)
        {
            data[0] = row.data[0];
            data[1] = row.data[1];
            data[2] = row.data[2];
            data[3] = row.data[3];
            data[4] = row.data[4];
            data[5] = row.data[5];
            data[6] = row.data[6];
            data[7] = row.data[7];
            data[8] = row.data[8];
            data[9] = row.data[9];
            data[10] = row.data[10];
        }

    }
    static class Class1
    {
        static string path = "../../data.txt";
        static List<double>[] data = new List<double>[11];
        static public List<Row> rows = new List<Row>();
        static public List<Row> rows2 = new List<Row>();
    public static void ReadData()
        {
            rows.Clear();
            for (int i = 0; i < 11; i++)
                data[i] = new List<double>();
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    rows.Add(new Row(sr.ReadLine()));
                }
                foreach (Row row in rows)
                {
                    for (int i = 0; i < 11; i++)
                        if (!data[i].Contains(row.data[i]))
                            data[i].Add(row.data[i]);

                }
            }
        }
        public static double MI(List<Row> r, int Column1, int Column2)
        {

            double[] val1 = Diskr(r, Column1);
            double[] val2 = Diskr(r, Column2);
            double[,] res = new double[val2.Length, val1.Length];

            foreach (Row row in r)
            {
                int i = GetInterval(row.data[Column1], val1);
                int j = GetInterval(row.data[Column2], val2);
                res[j, i]++;
            }
            
            //int size = data[Column1].Count;
            for (int i = 0; i < val1.Length; i++)
                for(int j=0; j< val2.Length; j++)
                    res[j, i] /= 1000;
            
            double[] P = new double[val1.Length];
            double[] P1 = new double[val2.Length];
            double[,] res2 = new double[val2.Length, val1.Length];
            for (int i = 0; i < val1.Length; i++)            
                for (int j = 0; j < val2.Length; j++) {
                    P1[j] += res[j, i];
                    P[i] += res[j, i];
                }
            
            for (int i = 0; i < val1.Length; i++)
                for (int j = 0; j < val2.Length; j++)
                    if (res[j, i] > 0.00001)
                        res2[j, i] = res[j, i] * Math.Log(res[j, i] / (P[i] * P1[j]), 2);
            
            double res1 = 0;
            for (int i = 0; i < val1.Length; i++)
                for (int j = 0; j < val2.Length; j++) 
                    res1 += res2[j, i];

            return res1;
        }
        public static void Shuffle(int Column)
        {
            rows2.Clear();
            for (int i = 0; i < rows.Count; i++)
            {
                rows2.Add(new Row( rows[i]));
            }
            Random random = new Random();
            int m = 500;
            int j, k;
            for (int i = 0; i < m; i++)
            {
                j = random.Next(0, 1000);
                k = random.Next(0, 1000);
                (rows2[j].data[Column], rows2[k].data[Column]) = (rows2[k].data[Column], rows2[j].data[Column]);
            }
        }
        public static double Mix(int Column1, int Column2)
        {
            double res = 0;
            int m = 100;
            for (int i = 0; i < m; i++)
            {
                Shuffle(Column1);
                res = Math.Max(res, MI(rows2, Column1, Column2));
            }
            return res;
        }
        public static double[] Diskr(List<Row> r, int Column)
        {

            List<double> d = new List<double>();
            double[] res = new double[r.Count];

            if (Column == 1 || Column == 2 || Column == 6 || Column == 8 || Column == 9 || Column == 10) {
                for(int i=0; i<data[Column].Count; i++)
                    d.Add(data[Column][i]);
                d.Sort();
            }
            else
            {
                for (int i = 0; i < r.Count; i++)
                    res[i] = r[i].data[Column];

                double temp;
                for (int i = 0; i < res.Length - 1; i++)
                {
                    for (int j = i + 1; j < res.Length; j++)
                    {
                        if (res[i] > res[j])
                        {
                            temp = res[i];
                            res[i] = res[j];
                            res[j] = temp;
                        }
                    }
                }

                double ch = 0;
                for (int i = 0; i < res.Length; i += 200)
                    if (res[i] != ch)
                    {
                        d.Add(res[i]);
                        ch = res[i];
                    }
                d.Add(res.Last());
            }

            res = new double[d.Count];
            for (int i = 0; i < d.Count; i++)
                res[i] = d[i];
            return res;
        }
        public static int GetInterval(double val, double[] sep)
        {
            if (val <= sep[0])
                return 0;
            for (int i = 1; i < sep.Length; i++)
                if (val <= sep[i])
                    return i;
            return -1;
        }
    }
}
