﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Sand
{
    static class Class2
    {
        static string path = "../../data_Quality.txt";
        public static List<Row> Quality = new List<Row>();
        static List<double>[] data = new List<double>[11];
        static double[,] matrix;
        static List<int> SK = new List<int>();
        static bool[] churn;
        public static double[,] Simmetric_coef = new double[11, 11];
        public static void Build_coef()
        {

            for (int i = 0; i < 11; i++)
                for (int j = 0; j < 11; j++)
                {
                    Program.MI[i, j] = MI(i, j);
                    Simmetric_coef[i, j] = 2 * Program.MI[i, j] / (Program.MI[i, i] + Program.MI[j, j]);
                }
        }
        public static double MI(int Column1, int Column2)
        {

            double[] val1 = Diskr(Column1);
            double[] val2 = Diskr(Column2);
            double[,] res = new double[val2.Length, val1.Length];

            foreach (Row row in Quality)
            {
                int i = GetInterval(row.data[Column1], val1);
                int j = GetInterval(row.data[Column2], val2);
                res[j, i]++;
            }

            //int size = data[Column1].Count;
            for (int i = 0; i < val1.Length; i++)
                for (int j = 0; j < val2.Length; j++)
                    res[j, i] /= 1000;

            double[] P = new double[val1.Length];
            double[] P1 = new double[val2.Length];
            double[,] res2 = new double[val2.Length, val1.Length];
            for (int i = 0; i < val1.Length; i++)
                for (int j = 0; j < val2.Length; j++)
                {
                    P1[j] += res[j, i];
                    P[i] += res[j, i];
                }

            for (int i = 0; i < val1.Length; i++)
                for (int j = 0; j < val2.Length; j++)
                    if (res[j, i] > 0.00001)
                        res2[j, i] = res[j, i] * Math.Log(res[j, i] / (P[i] * P1[j]), 2);

            double res1 = 0;
            for (int i = 0; i < val1.Length; i++)
                for (int j = 0; j < val2.Length; j++)
                    res1 += res2[j, i];

            return res1;
        }
        public static void Build_Quality()
        {
            Quality.Clear();
            for (int i = 0; i < 11; i++)
                data[i] = new List<double>();
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    Quality.Add(new Row(sr.ReadLine()));
                }
                foreach (Row row in Quality)
                {
                    for (int i = 0; i < 11; i++)
                        if (!data[i].Contains(row.data[i]))
                            data[i].Add(row.data[i]);

                }
                for (int i = 0; i < 11; i++)
                {
                    data[i].Sort();
                }
            }
            Build_coef();
        }
        static void Copy(List<int> to, List<int> from)
        {
            to.Clear();
            for (int i = 0; i < from.Count; i++)
                to.Add(from[i]);
        }
        public static List<int> Sk()
        {
            using (StreamWriter sr = new StreamWriter("../../ss.txt"))
            {
                List<int> res = new List<int>(), tmp = new List<int>();
                double max = 0;
                for (int i = 1; i < Math.Pow(2, 10); i++)
                {
                    tmp.Clear();
                    int j = i;
                    for (int k = 0; k < 10; k++)
                    {
                        if (j % 2 == 1)
                            tmp.Add(k);
                        j /= 2;
                    }
                    double t = F(tmp);
                    if (t > max)
                    {
                        Copy(res, tmp);
                        max = t;
                    }
                    sr.WriteLine(t + "\t" + tmp.Count);
                }
                return res;
            }
        }
        public static void Beis()
        {
            Build_Quality();
            SK = Sk();
            List<double[,]> sopr = new List<double[,]>();
            foreach (int t in SK)
                sopr.Add(Sopr(Quality, t, 10));
            matrix = Max_prov(sopr);
            churn = Churn();
            using (StreamWriter sr = new StreamWriter("../../churn.txt"))
            {
                for (int i = 900; i < 1000; i++)
                {
                    if (Max_Prav(Quality[i], sopr) != (Quality[i].revenue == 1))
                        sr.WriteLine((Quality[i].revenue != 1) + "\t" + (Quality[i].revenue == 1));
                }
            }
            churn = Churn1();
            using (StreamWriter sr = new StreamWriter("../../churn1.txt"))
            {
                for (int i = 900; i < 1000; i++)
                {
                    if (Max_Prav(Quality[i], sopr) != (Quality[i].revenue == 1))
                        sr.WriteLine((Quality[i].revenue != 1) + "\t" + (Quality[i].revenue == 1));
                }
            }
            using (StreamWriter sr = new StreamWriter("../../res2.txt"))
            {
                churn = Churn();
                int[,] tmp = new int[2, 2];
                for (int i = 900; i < 1000; i++)
                {
                    bool a = Max_Prav(Quality[i], sopr);
                    bool b = Quality[i].revenue == 1;
                    if (a && b)
                        tmp[0, 0]++;
                    else if (a && !b)
                        tmp[1, 0]++;
                    else if (!a && b)
                        tmp[0, 1]++;
                    else
                        tmp[1, 1]++;
                }
                double acc = (double)(tmp[0, 0] + tmp[1, 1]) / 100;
                double tpr = (double)tmp[0, 0] / (tmp[0, 0] + tmp[0, 1]);
                double tnr = (double)tmp[1, 1] / (tmp[1, 0] + tmp[1, 1]);
                double err = 1 - acc;
                double fpr = 1 - tnr;
                double fnr = 1 - tpr;
                double prec = (double)tmp[0, 0] / (tmp[0, 0] + tmp[1, 0]);
                sr.WriteLine("Верность = " + acc + " Чувствительность = " + tpr + " Специфичность = "
                    + tnr + " Частота ошибок = " + err+ " Частота ложноположительных результатов = "
                    + fpr+ " Частота ложноотрицательных результатов " + fnr+ " Точность (доверие) = " + prec);

                churn = Churn1();
                tmp = new int[2, 2];
                for (int i = 900; i < 1000; i++)
                {
                    bool a = Max_Prav(Quality[i], sopr);
                    bool b = Quality[i].revenue == 1;
                    if (a && b)
                        tmp[0, 0]++;
                    else if (a && !b)
                        tmp[1, 0]++;
                    else if (!a && b)
                        tmp[0, 1]++;
                    else
                        tmp[1, 1]++;
                }
                acc = (double)(tmp[0, 0] + tmp[1, 1]) / 100;
                tpr = (double)tmp[0, 0] / (tmp[0, 0] + tmp[0, 1]);
                tnr = (double)tmp[1, 1] / (tmp[1, 0] + tmp[1, 1]);
                err = 1 - acc;
                fpr = 1 - tnr;
                fnr = 1 - tpr;
                prec = (double)tmp[0, 0] / (tmp[0, 0] + tmp[1, 0]);
                sr.WriteLine("Верность = " + acc + " Чувствительность = " + tpr + " Специфичность = "
                    + tnr + " Частота ошибок = " + err + " Частота ложноположительных результатов = "
                    + fpr + " Частота ложноотрицательных результатов " + fnr + " Точность (доверие) = " + prec);

            }

        }
        public static bool[] Churn()
        {
            bool[] res = new bool[matrix.Length / 2];
            for (int i = 0; i < res.Length; i++)
                res[i] = matrix[i, 0] < matrix[i, 1];
            return res;
        }
        public static bool[] Churn1()
        {
            int tr = 0;
            for (int i=0;i<900;i++)
                if (Quality[i].revenue == 1)
                    tr++;
            tr++;
            bool[] res = new bool[matrix.Length / 2];
            for (int i = 0; i < res.Length; i++)
                res[i] = matrix[i, 0] * ((902.0 - tr) / 902.0) < matrix[i, 1] * (tr / 902.0);
            return res;
        }
        public static bool Max_Prav(Row a, List<double[,]> sopr)
        {
            List<int> indices = new List<int>();
            for (int i = 0; i < SK.Count; i++)
            {
                double[] sep = Diskr(SK[i]);
                indices.Add(GetInterval(a.data[SK[i]], sep));
            }
            return churn[ToIndex(indices, sopr)];
        }
        private static double[,] Max_prov(List<double[,]> sopr)
        {
            int size = 1;
            foreach (double[,] doub in sopr)
            {
                size *= doub.Length / 2;
            }
            double[,] max_prov = new double[size, 2];
            for (int i = 0; i < size; i++)
            {
                max_prov[i, 0] = 1;
                max_prov[i, 1] = 1;
            }
            List<int> index;
            for (int i = 0; i < size; i++)
            {
                index = FromIndex(i, sopr);
                for (int j = 0; j < sopr.Count; j++)
                {
                    max_prov[i, 0] *= sopr[j][index[j], 0];
                    max_prov[i, 1] *= sopr[j][index[j], 1];
                }
            }
            return max_prov;
        }
        public static int ToIndex(List<int> indices, List<double[,]> sopr)
        {
            int res = 0;
            int pow = 1;
            for (int i = 0; i < indices.Count; i++)
            {
                res += indices[i] * pow;
                pow *= sopr[i].Length / 2;
            }
            return res;
        }
        public static List<int> FromIndex(int index, List<double[,]> sopr)
        {
            List<int> res = new List<int>();
            int pow = 1;
            for (int i = 0; i < sopr.Count; i++)
            {
                pow *= sopr[i].Length / 2;
                res.Add(index % pow);
                index /= pow;
            }
            return res;
        }
        public static double F(List<int> Columns)
        {
            double res = 0;
            int k = Columns.Count();
            for (int i = 0; i < k; i++)
                res += Simmetric_coef[Columns[i], 10];
            double tmp = 0;
            for (int i = 0; i < k; i++)
                for (int j = i + 1; j < k; j++)
                    tmp += Simmetric_coef[Columns[i], Columns[j]];
            res /= Math.Sqrt(k + 2 * tmp);
            return res;
        }
        public static double[] Diskr(int Column)
        {
            List<double> d = new List<double>();
            double[] res;
            for (int i = 0; i < data[Column].Count; i++)
                d.Add(data[Column][i]);
            d.Sort();
            res = new double[d.Count];
            for (int i = 0; i < d.Count; i++)
                res[i] = d[i];
            return res;
        }
        public static double[,] Sopr(List<Row> r, int Column1, int Column2)
        {
            double[] val1 = Diskr(Column1);
            double[] val2 = Diskr(Column2);
            double[,] res = new double[val1.Length, val2.Length];
            foreach (Row row in r)
            {
                int i = GetInterval(row.data[Column1], val1);
                int j = GetInterval(row.data[Column2], val2);
                res[i, j]++;
            }
            for (int i = 0; i < val2.Length; i++)
            {
                double sum = 0;
                for (int j = 0; j < val1.Length; j++)
                    sum += res[j, i] + 1;
                for (int j = 0; j < val1.Length; j++)
                    res[j, i] = (res[j, i] + 1) / sum;
            }
            return res;
        }
        public static int GetInterval(double val, double[] sep)
        {
            for (int i = 0; i < sep.Length; i++)
                if (val <= sep[i])
                    return i;
            return -1;
        }
    }
}